import {ajax} from "./common.js";
import Inputmask from "inputmask";
import popup from "./popup.js";

function PopupRegistration() {
  //const block = document.querySelector('.popup--registration');
  const block = false;
  if (!block) return;

  this.html = {
    block: block,
    form: block.querySelector('.popup__form'),
    inputs: block.querySelectorAll('.popup__input'),
    agreement: block.querySelector('.agreement'),
    agreementCheckbox: block.querySelector('.agreement__input'),
    activators: document.querySelectorAll('[data-popup="registration"]'),
  }
  this.options = {
    phoneComlite: localStorage.getItem('phone') ? true : false,
  }



  this.init = () => {
    this.initActivators();
    this.getStorageValues();
    this.makeInputMasks();
    this.initSubmit();
    this.initAgreement();
  }


  this.initActivators = () => {
    Array.from(this.html.activators).forEach((btn) => {
      btn.onclick = (e) => {
        e.preventDefault();
        popup.openPopup(this.html.block);
      }
    });
  }
  this.initSubmit = () => {
    this.html.form.onsubmit = (e) => {
      e.preventDefault();

      const formData = new FormData(this.html.form);
      if (this.html.agreementCheckbox.checked) {
        ajax(this.html.form.method  || 'GET', this.html.form.action, this.checkResponse, formData);
      } else {
        this.html.agreement.classList.add('agreement--error');
      }
    }
  }
  this.initAgreement = () => {
    this.html.agreementCheckbox.oninput = () => {
      this.html.agreement.classList.remove('agreement--error');
    }
  }
  this.checkResponse = (response) => {
    const obj = JSON.parse(response);
    if (obj.result == 'success') {
      this.showSuccess();
      this.setStorageValues();
    }
  }
  this.showSuccess = () => {
    popup.closePopup(this.html.block);
    popup.openPopup(document.querySelector('.popup--registration-success'));
  }


  this.getStorageValues = () => {
    Array.from(this.html.inputs).forEach((input) => {
      let storageValue = localStorage.getItem('' + input.name);
      if (!storageValue) return;
      input.value = storageValue;
    });
  }
  this.setStorageValues = () => {
    Array.from(this.html.inputs).forEach((input) => {
      localStorage.setItem('' + input.name, input.value);
    });
  }
  this.makeInputMasks = () => {
    const phone = this.html.form.querySelector('.popup__input--phone');
    Inputmask({
      "mask": "+7 (999) 999-9999",
      oncomplete: () => {
        this.options.phoneComplete = true;
      },
      onincomplete: () => {
        this.options.phoneComplete = false;
      }
    }).mask(phone);
  }



  this.init();
}



export default new PopupRegistration();