import Inputmask from "inputmask";

function ajax(method='GET', url, callback, data) {
  var xhr = new XMLHttpRequest();
  xhr.open(method, url, true);

  xhr.onload = function() {
    if (this.status == 200) {
      return callback(this.response);
    } else {
      var error = new Error(this.statusText);
      error.code = this.status;
      console.log(error);
    }
  };

  xhr.onerror = function() {
    console.log(new Error("Network Error"));
  };
    xhr.setRequestHeader('X-REQUESTED-WITH', 'XMLHttpRequest');
    xhr.send(data);
}



// склонение слова от числа
function wordsDeclension(num, expressions) {
  let result;
  let count = num % 100;

  if (count >= 5 && count <= 20) {
    result = expressions['2'];
  } else {
    count = count % 10;
    if (count == 1) {
      result = expressions['0'];
    } else if (count >= 2 && count <= 4) {
      result = expressions['1'];
    } else {
      result = expressions['2'];
    }
  }
  return result;
}



function clickOutside(e, tagClass) {
  const target = e.target;

  function checkElement(elem) {
    if (elem.classList && elem.classList.contains(tagClass)) {
      return true;
    }

    if (elem.parentNode) {
      return checkElement(elem.parentNode);
    }

    return false;
  }

  return checkElement(target);
}

function phoneInputMask() {
    this.init = function () {
        const phones = document.querySelectorAll('.js-phone');
        [...phones].map(phone => {
            if (phone) {
                let options = {
                    page: 999,
                };

                Inputmask({
                    "mask": "+7 (999) 999-9999",
                }).mask(phone);
            }
        })
    }
}

window.mask = new phoneInputMask();

export {ajax, wordsDeclension, clickOutside};