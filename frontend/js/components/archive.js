function Accordion() {
  const accordion = document.querySelector('.accordion');

  this.init = () => {
    if (accordion) {
      accordion.addEventListener('click', (e) => {
        let currentElement = e.target;
        currentElement.classList.contains('accordion__title')
          && currentElement.parentElement.classList.toggle('accordion__item--active');
             currentElement.nextElementSibling.classList.toggle('info--active');
      });
    }
  }
  
  this.init();
}

export default new Accordion();