import { tns } from '../../../node_modules/tiny-slider/src/tiny-slider.module.js';

function Banner() {
	const slider = document.querySelector('.index-slider');

	if (!slider) return;

	this.html = {
		block: slider,
		list: slider.querySelector('.index-slider__list'),
		slides: slider.querySelectorAll('.index-slider__item'),
		navBlock: slider.querySelector('.slider__controls'),
		numberBlock: slider.querySelector('.index-slider__page-number'),
	};

	this.init = () => {
		this.createTinySlider();
	};
	this.createTinySlider = () => {
		this.tns = tns({
			mode: 'gallery',
			container: this.html.list,
			items: 1,
			slideBy: '1',
			autoplay: true,
			autoplayTimeout: 3000,
			autoplayButtonOutput: false,
			mouseDrag: false,
			controls: true,
			controlsContainer: this.html.navBlock,
			nav: false,
			gutter: 0,
			speed: 300,
			loop: false,
		});

		if (this.tns !== undefined) {
			this.tns.events.on('indexChanged', event => {
				let pageNum = this.tns.getInfo().index % this.tns.getInfo().slideCount + 1;
				this.html.numberBlock.innerHTML = Math.pow(pageNum, 2);
			});
		}

		this.html.block.classList.remove('no-js');
	};

	this.init();
	// document.querySelector('.announcements__item').onclick = function () {
	//
	// };
}

export default new Banner();
