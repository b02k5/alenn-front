function TabBlock() {
  const block = document.querySelector('.tab-block');
  if (!block) return;

  this.html = {
    block: block,
    toggles: block.querySelectorAll('.tab-block__name'),
    sections: block.querySelectorAll('.sections__item')
  }

  this.options = {
    active: 0
  }



  this.init = () => {
    this.initToggles();
  }
  this.initToggles = () => {
    for (let i=0; i<this.html.toggles.length; i++) {
      const toggle = this.html.toggles[i];

      toggle.onclick = (e) => {
        e.preventDefault();
        if (i == this.options.active) return;

        this.changeActive(i);
      }
    }
  }
  this.changeActive = (number) => {
    this.html.toggles[this.options.active].classList.remove('tab-block__name--active');
    this.html.sections[this.options.active].classList.remove('sections__item--active');

    this.options.active = number;

    this.html.toggles[this.options.active].classList.add('tab-block__name--active');
    this.html.sections[this.options.active].classList.add('sections__item--active');
  }



  this.init();
}



export default new TabBlock();