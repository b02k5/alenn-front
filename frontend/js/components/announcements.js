import { tns } from "../../../node_modules/tiny-slider/src/tiny-slider.module.js";

function Announcements() {
  const slider = document.querySelector('.announcements');
  const sliderItems = document.querySelector('.announcements__item');
  const controls = document.querySelector('.announcements__controls');

  if (!slider) return;

  this.html = {
    block: slider,
    list: slider.querySelector('.announcements__list'),
    slides: slider.querySelectorAll('.announcements__item'),
    navBlock: slider.querySelector('.announcements__controls'),
  }
  this.inited = true;


  this.init = () => {
    if(this.html.list.childElementCount <= 1)
      return;
    this.createTinySlider();
    // this.checkCountItems();
  }

  this.checkCountItems = () => {
    console.log(sliderItems.length)
  };

  this.createTinySlider = () => {

    this.tns = tns({
      container: this.html.list,
      items: 1,
      slideBy: '1',
      autoplay: false,
      mouseDrag: false,
      controls: true,
      controlsContainer: this.html.navBlock,
      nav: false,
      gutter: 30,
      speed: 300,
      loop: false,
      responsive: {
        768: {
          items: 2
        }
      }
    });

    const info = this.tns.getInfo();
    info.slideItems[info.index].classList.add('announcements__item--active');
    this.myIndex = info.index;

    this.html.block.classList.remove('no-js');

    if (info.slideCount > 2 || info.items === 1) {
      controls.classList.add('announcements__controls--active');
    }

    this.tns.events.on('indexChanged', (event) => {
      const info = this.tns.getInfo();

      info.slideItems[this.myIndex].classList.remove('announcements__item--active');
      this.myIndex = info.index;
      info.slideItems[this.myIndex].classList.add('announcements__item--active');
    });
  }



  this.init();
}

export default new Announcements();