function Direction() {
  const direction = document.querySelector('[data-type="direction"]');
  const resultList = document.querySelector('.result__list');

  if (!direction) return;

  this.init = () => {
    resultList.addEventListener('click', (e) => {
      const currentElement = e.target.closest('.result__item');
      window.location = currentElement.getAttribute('data-location');
    });    
  }

  this.init();
}



export default new Direction();