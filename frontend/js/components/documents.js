import { tns } from "../../../node_modules/tiny-slider/src/tiny-slider.module.js";

function DocumentsSlider() {
  const block = document.querySelector('.documents');
  if (!block) return;

  this.html = {
    block: block,
    lists: block.querySelectorAll('.documents__list'),
    controls: block.querySelectorAll('.slider__controls')
  }
  this.tns = [];



  this.init = () => {

    for (let i=0; i<this.html.lists.length; i++) {
      if(this.html.lists[i].childElementCount > 1){
          this.createTinySlider(i);
      }else{
          this.html.lists[i].parentElement.classList.add('documents__block--no-slider');
      }
    }
  }
  this.createTinySlider = (number) => {

    this.tns.push(tns({
      container: this.html.lists[number],
      items: 1,
      slideBy: '1',
      autoplay: false,
      mouseDrag: false,
      controls: true,
      controlsContainer: this.html.controls[number],
      nav: false,
      gutter: 30,
      edgePadding: 15,
      speed: 300,
      loop: false,
      responsive: {
        768: {
          items: 3,
          edgePadding: 0,
        },
        1300: {
          items: 4,
        }
      }
    }));
    this.html.block.classList.remove('no-js');
  }



  this.init();
}



export default new DocumentsSlider();