import { tns } from "../../../node_modules/tiny-slider/src/tiny-slider.module.js";
import { ajax } from './common.js';

function Achievements() {
  const slider = document.querySelector('.achievements');
  if (!slider) return;

  this.html = {
    block: slider,
    list: slider.querySelector('.achievements__list'),
    slides: slider.querySelectorAll('.achievements__item'),
    more: slider.querySelector('.achievements__more'),
  }
  this.ajaxOptions = {
    url: '../json/achievements.json',
    method: 'GET'
  }



  this.init = () => {
    // На главной странице нужен слайдер
    if (this.html.block.classList.contains('achievements--index')) {
      this.createTinySlider();
    }

    if (this.html.more) {
      this.createTemplate();
      this.initLoader();
    }
  }
  this.createTinySlider = () => {
    this.tns = tns({
      container: this.html.list,
      items: 1,
      slideBy: '1',
      autoplay: false,
      mouseDrag: false,
      controls: false,
      nav: false,
      gutter: 30,
      speed: 300,
      loop: false,
      edgePadding: 15,
      responsive: {
        768: {
          disable: true
        }
      }
    });
  }


  this.createTemplate = () => {
    const li = document.createElement('li');
    li.classList.add('achievements__item');

    const a = document.createElement('a');
    a.classList.add('achievements__link');

    const imgWrapper = document.createElement('div');
    imgWrapper.classList.add('achievements__img-wrapper');

    const img = document.createElement('img');
    img.classList.add('achievements__img');
    imgWrapper.appendChild(img);
    a.appendChild(imgWrapper);

    const date = document.createElement('time');
    date.classList.add('achievements__date');
    a.appendChild(date);

    const name = document.createElement('p');
    name.classList.add('achievements__name');
    a.appendChild(name);

    const description = document.createElement('p');
    description.classList.add('achievements__description');
    a.appendChild(description);
    li.appendChild(a);

    this.template = li.cloneNode(true);
  }
  this.initLoader = () => {
    // this.html.more.onclick = (e) => {
    //   e.preventDefault();
    //   ajax(this.ajaxOptions.method, this.ajaxOptions.url, this.checkResponse);
    // }
  }
  this.checkResponse = (response) => {
    const obj = JSON.parse(response);
    const dataArr = obj.data || [];
    const items = (!dataArr || !dataArr[0]) ? [] : dataArr[0];
    const more = obj.more;

    this.addItems(items);
    if (!more) this.html.more.classList.add('achievements__more--disabled');
  }
  this.addItems = (items) => {
    for (let i in items) {
      const item = items[i];
      const template = this.template.cloneNode(true);


      const link = template.querySelector('.achievements__link');
      link.href = item.link;

      const img = template.querySelector('.achievements__img');
      img.src = item.img;
      img.alt = item.name;

      const date = template.querySelector('.achievements__date');
      date.innerHTML = item.date;

      const name = template.querySelector('.achievements__name');
      name.innerHTML = item.name;

      const description = template.querySelector('.achievements__description');
      description.innerHTML = item.name;


      this.html.list.appendChild(template);
    }
  }



  this.init();
}

export default new Achievements();