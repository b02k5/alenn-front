import { ajax } from './common.js';

function GalleryGroup() {
  const block = document.querySelector('.gallery-group');
  if (!block) return;

  this.html = {
    block: block,
    list: block.querySelector('.gallery-group__list'),
    more: block.querySelector('.gallery-group__more'),
  }
  this.ajaxOptions = {
    url: '../json/gallery-group.json',
    method: 'GET',
  }



  this.init = () => {
    this.createTemplate();
    this.initLoader();
  }


  this.createTemplate = () => {
    const li = document.createElement('li');
    li.classList.add('gallery-group__item');

    const a = document.createElement('a');
    a.classList.add('gallery-group__link');

    const info = document.createElement('div');
    info.classList.add('gallery-group__info');

    const date = document.createElement('time');
    date.classList.add('gallery-group__date');
    info.appendChild(date);

    const count = document.createElement('time');
    count.classList.add('gallery-group__count');
    info.appendChild(count);
    a.appendChild(info);

    const imgWrapper = document.createElement('div');
    imgWrapper.classList.add('gallery-group__img-wrapper');

    const img = document.createElement('img');
    img.classList.add('gallery-group__img');
    imgWrapper.appendChild(img);
    a.appendChild(imgWrapper);

    const name = document.createElement('p');
    name.classList.add('gallery-group__name');
    a.appendChild(name);

    li.appendChild(a);

    this.template = li.cloneNode(true);
  }
  this.initLoader = () => {
    // this.html.more.onclick = (e) => {
    //   e.preventDefault();
    //   ajax(this.ajaxOptions.method, this.ajaxOptions.url, this.checkResponse);
    // }
  }
  this.checkResponse = (response) => {
    const obj = JSON.parse(response);
    const dataArr = obj.data || [];
    const items = (!dataArr || !dataArr[0]) ? [] : dataArr[0];
    const more = obj.more;

    this.addItems(items);
    if (!more) this.html.more.classList.add('gallery-group__more--disabled');
  }
  this.addItems = (items) => {
    for (let i in items) {
      const item = items[i];
      const template = this.template.cloneNode(true);


      const link = template.querySelector('.gallery-group__link');
      link.href = item.link;

      const date = template.querySelector('.gallery-group__date');
      date.innerHTML = item.date;

      const count = template.querySelector('.gallery-group__count');
      count.innerHTML = item.count;

      const img = template.querySelector('.gallery-group__img');
      img.src = item.img;
      img.alt = item.name;

      const name = template.querySelector('.gallery-group__name');
      name.innerHTML = item.name;


      this.html.list.appendChild(template);
    }
  }



  this.init();
}

export default new GalleryGroup();