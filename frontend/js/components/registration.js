import Choices from "../../../node_modules/choices.js/assets/scripts/dist/choices.min.js";
import autoComplete from "../../../node_modules/js-autocomplete/auto-complete.min.js";
import { ajax } from "./common.js";
import Inputmask from "inputmask";
import popup from "./popup.js";

function Registration() {
    const block = document.querySelector('.registration');

    if (!block) return;

    this.html = {
        block: block,
        otherSchoolName: block.querySelector('.registration__input-block--otherSchoolName'),
        noSchool: block.querySelector('.registration__label--checkbox'),
        inputAddress: block.querySelector('.registration__input--address'),
        step: block.querySelector('.registration__step-current'),
        pages: block.querySelectorAll('.registration__page'),
        form: block.querySelector('.registration__form'),
        result: block.querySelector('.registration-check'),
        pagePrev: block.querySelector('.registration__control-prev'),
        pageNext: block.querySelector('.registration__control-next'),
        submit: block.querySelector('.registration__control-submit'),
        agreement: block.querySelector('.agreement'),
        agreementCheckbox: block.querySelector('.agreement__input'),

        registrationInputTerritorial: block.querySelector('.registration__input--address'),
        registrationInputSchool: block.querySelector('.registration__input--institution'),
        registrationHideBlocks: block.querySelectorAll('.registration__fieldset--hide'),
        registrationInputPhones: block.querySelectorAll('.js-phone'),
        registrationButtonNotSchool: block.querySelector('.registration__control-not-school')
    };

    this.options = {
        page: 0,
        phoneComplete: false,
        isSchoolOpen: false,
    };

    this.ajaxOptions = {
        url: '/system/ajax/getschoollist.php',
        method: 'GET',
    };

    this.init = () => {
        this.initSelectors();
        this.initFields();
        this.initPageChangers();
        this.initSubmit();
        this.initAgreement();
        this.initPhoneMask();
        this.getDataJson();

        this.html.registrationInputSchool.parentNode.style.display = 'none';
        this.html.pageNext.style.display = 'none';
        this.html.otherSchoolName.parentNode.style.display = 'none';
        this.html.registrationButtonNotSchool.style.display = 'none';
    };

    this.initSelectors = () => {
        const selects = this.html.form.querySelectorAll('.registration__select');
        Array.from(selects).forEach((item) => {
            const choice = new Choices(item, {
                searchEnabled: false,
                itemSelectText: '',
                shouldSort: false,
            })
        });
    };

    this.initFields = () => {
        const fields = this.html.form.querySelectorAll('[data-field]');
        this.fields = {};

        Array.from(fields).forEach((item) => {
            const dataField = item.getAttribute('data-field');

            this.fields[dataField] = {
                input: item,
                result: this.html.result.querySelector('[data-field="' + dataField + '"]')
            };

            item.onfocus = () => {
                item.parentNode.classList.remove('registration__input-block--error');
            };

            item.onblur = () => {
                // если это селект, надо записать текст у нужного option
                if (item.classList.contains('registration__select')) {
                    this.fields[dataField].result.innerHTML = item.querySelector('option[value="' + item.value + '"]').innerHTML;
                    return;
                }
                // если пустой класс, то записываем 0
                if (item.parentNode.classList.contains('registration__input-block--class') && item.value === '') {
                    this.fields[dataField].result.innerHTML = 0;
                    return;
                }

                if (this.fields[dataField].result) {
                    this.fields[dataField].result.innerHTML = item.value;
                }
            };
        });

        this.getStorageValues();
    };

    // переключение страниц
    this.initPageChangers = () => {
        if (!this.html.pagePrev || !this.html.pageNext) return;

        this.html.pagePrev.onclick = (e) => {
            e.preventDefault();
            this.changePage(this.options.page - 1);
        };

        this.html.pageNext.onclick = (e) => {
            e.preventDefault();

            if (this.checkInputs()) {
                return;
            }

            const select = block.querySelector('.registration__select');
            const valueField = block.querySelector(`.registration-check__value[data-field='field_contest']`);

            valueField.innerHTML = select.options[select.selectedIndex].text;

            this.changePage(this.options.page + 1);
        }
    };

    this.changePage = (pageNumber) => {
        if (pageNumber < 0) {
            return;
        }

        this.html.pages[this.options.page].classList.remove('registration__page--active');
        this.options.page = pageNumber;

        this.html.pages[this.options.page].classList.add('registration__page--active');
        this.html.step.innerHTML = pageNumber + 1;
    };

    this.checkInputs = () => {
        let errors = false;

        let regexEmail = /^[0-9a-zA-Z._-]+@[0-9a-zA-Z-.]+\.[a-z]{2,}/;

        const allInputFromBlock = block.querySelectorAll('[data-field]');

        Array.from(allInputFromBlock).forEach((input) => {
            if (!input.hasAttribute('required')) {
                return;
            }

            let inputAttribute = input.getAttribute('data-field');

            if (inputAttribute === 'field_mail' && regexEmail.test(input.value)) {
                return;
            } else if (inputAttribute === 'field_phone' && (input.inputmask && this.options.phoneComplete === true)) {
                return;
            } else if (inputAttribute === 'field_fio' && input.value.length > 5) {
                return;
            }

            input.parentNode.classList.add('registration__input-block--error');
            errors = true;
        });

        return errors;
    };

    this.initAgreement = () => {
        this.html.agreementCheckbox.oninput = () => {
            this.html.agreement.classList.remove('agreement--error');
        }
    };

    // отправка формы
    this.initSubmit = () => {
        this.html.submit.onclick = (e) => {
            e.preventDefault();

            if (e.target.hasAttribute('disabled') === true) {
                return false;
            }

            const formData = new FormData(this.html.form);
            if (this.html.agreementCheckbox.checked) {
                e.target.setAttribute('disabled', 'disabled');
                e.target.innerText = 'Отправляется ...';
                event.target.style.color = '#000';

                ajax(this.html.form.method || 'GET', this.html.form.action, this.checkResponse, formData);
            } else {
                this.html.agreement.classList.add('agreement--error');
            }
        };

        this.html.registrationButtonNotSchool.addEventListener('click', (event) => {
            event.preventDefault();

            if (event.target.hasAttribute('disabled') === true) {
                return;
            }

            if (this.html.otherSchoolName.value.length < 3) {
                alert('Укажите вашу школу');
                return;
            }

            for (let i in this.fields) {
                const dataAttribute = this.fields[i].input.getAttribute('data-field');
                const registrationInputFromDataAttribute = document.querySelector(`input[data-field=${dataAttribute}]`);

                if (dataAttribute === 'field_fio') {
                    registrationInputFromDataAttribute.value = 'Не заполнено';
                } else if (dataAttribute === 'field_mail') {
                    registrationInputFromDataAttribute.value = 'notify@example.com';
                } else if (dataAttribute === 'field_phone') {
                    registrationInputFromDataAttribute.value = '+7 (000) 000-00-00'
                } else if (dataAttribute === 'field_number') {
                    registrationInputFromDataAttribute.value = 'Не найдено';
                }
            }

            this.html.agreementCheckbox.setAttribute('checked', 'checked');

            let tempTargetValue = event.target.innerText;

            event.target.setAttribute('disabled', 'disabled');
            event.target.innerText = 'Отправляется ...';
            event.target.style.color = '#000';

            const formData = new FormData(this.html.form);

            ajax(this.html.form.method || 'GET', this.html.form.action, (response) => {
                const registrationContent = document.querySelector('#reg_content');
                registrationContent.innerHTML = `
                    <div class="popup--registration-success">
                        <p class="popup__header">Спасибо!</p>
                        <p class="popup__text">
                            <font class="notetext">Мы внесем Вашу школу в список и через 1 сутки Вы сможете отправить заявку с сайта.</font>
                        </p>
                    </div>
                `;

                event.target.removeAttribute('disabled');
                event.target.innerText = tempTargetValue;
            }, formData);
        });
    };

    this.checkResponse = (response) => {
        const registrationContent = document.querySelector('#reg_content');
        registrationContent.innerHTML = response;
    };

    this.showSuccess = () => {
        popup.openPopup(document.querySelector('.popup--registration-success'))
    };

    this.getStorageValues = () => {
        for (let i in this.fields) {
            let item = this.fields[i];
            let input = item.input;
            let result = item.result;
            let storageValue = localStorage.getItem('' + input.name);

            if (!input.required || !storageValue) continue;

            input.value = storageValue;
            result.innerHTML = storageValue;
        }
    };

    this.setStorageValues = () => {
        for (let i in this.fields) {
            let item = this.fields[i];
            let input = item.input;

            if (!input.required) continue;

            localStorage.setItem('' + input.name, input.value);
        }
    };

    this.getDataJson = () => {
        ajax(this.ajaxOptions.method, this.ajaxOptions.url, this.createEvent);
    };

    this.createEvent = (response) => {
        const parsedJsonResponse = JSON.parse(response);

        let autoCompleteInputAddress;

        let autoCompleteInputSchool;

        let cities = [];

        if (parsedJsonResponse !== undefined) {
            [].forEach.call(parsedJsonResponse.data, function (item, key) {
                if (cities.indexOf(item.city) === -1) {
                    cities.push(item.city);
                }
            });
        }

        this.html.registrationInputTerritorial.addEventListener('focusin', (event) => {
            if (autoCompleteInputAddress !== undefined) {
                autoCompleteInputAddress.destroy();
            }

            if (event.target) {
                autoCompleteInputAddress = this.autoCompleteInput(cities, event.target, 1);
            }
        });

        this.html.registrationInputTerritorial.addEventListener('focusout', (event) => {
            if (event.target.value.length === 0) {
                this.html.registrationInputSchool.parentNode.style.display = 'none';
                [].forEach.call(this.html.registrationHideBlocks, function (item, key) {
                    item.style.display = 'none';
                });
            }
        });

        this.html.registrationInputSchool.addEventListener('focusin', (event) => {
            let schoolList = [];

            [].forEach.call(parsedJsonResponse.data, (item, key) => {
                if (item.city.indexOf(this.html.registrationInputTerritorial.value) !== -1 && schoolList.indexOf(item.school) === -1) {
                    schoolList.push(item.school);
                }
            });

            if (schoolList.length !== 0) {
                if (autoCompleteInputSchool !== undefined) {
                    autoCompleteInputSchool.destroy();
                }

                autoCompleteInputSchool = this.autoCompleteInput(schoolList, event.target, 2, parsedJsonResponse);
            }
        });

        this.html.registrationInputSchool.addEventListener('focusout', (event) => {
            if (event.target.value.length === 0) {
                [].forEach.call(this.html.registrationHideBlocks, function (item, key) {
                    item.style.display = 'none';
                });
            }
        });

        this.html.noSchool.addEventListener('click', (e) => {
            if (this.html.otherSchoolName.parentNode.style.display === 'none' || this.html.otherSchoolName.parentNode.style.display === '') {
                this.html.otherSchoolName.parentNode.style.display = 'block';
                this.html.registrationButtonNotSchool.style.display = 'block';
                this.html.pageNext.style.display = 'none';

                this.html.otherSchoolName.value = '';

                [].forEach.call(this.html.registrationHideBlocks, function (item, key) {
                    item.style.display = 'none';
                });
            } else {
                this.html.otherSchoolName.parentNode.style.display = 'none';
                this.html.registrationButtonNotSchool.style.display = 'none';

                this.html.otherSchoolName.value = 'Не указано';

                if (this.html.registrationInputSchool.value.length !== 0 && this.options.isSchoolOpen) {
                    this.html.pageNext.style.display = 'block';
                    [].forEach.call(this.html.registrationHideBlocks, function (item, key) {
                        item.style.display = 'block';
                    });
                }
            }
        });
    };

    this.autoCompleteInput = (dataArray, target, step, json) => {
        const complete = new autoComplete({
            selector: target,
            minChars: 1,
            source: function (term, suggest) {
                term = term.toLowerCase();
                let choices = dataArray;
                let suggestions = [];
                for (let i = 0; i < choices.length; i++) {
                    if (~choices[i].toLowerCase().indexOf(term)) {
                        suggestions.push(choices[i]);
                    }
                }
                suggest(suggestions);
            },
            onSelect: (event, term, item) => {
                if (step === 1) {
                    this.html.registrationInputSchool.parentNode.style.display = 'block';
                } else if (step === 2) {
                    this.html.registrationInputSchool.addEventListener('change', (e) => {
                        for (let i = 0; i < json.data.length; i++) {
                            if (e.target.value === json.data[i].school.replace(/&quot;/g, '"')) {
                                [].forEach.call(this.html.registrationHideBlocks, function (item, key) {
                                    item.style.display = 'block';
                                })
                                this.html.pageNext.style.display = 'block'
                                this.options.isSchoolOpen = true;
                                break
                            } else {
                                [].forEach.call(this.html.registrationHideBlocks, function (item, key) {
                                    item.style.display = 'none';
                                })
                                this.html.pageNext.style.display = 'none'
                                this.options.isSchoolOpen = false;
                            }
                        }
                    });
                }
            }
        });

        return complete;
    };

    this.initPhoneMask = () => {
        Array.from(this.html.registrationInputPhones).forEach((inputPhone) => {
            Inputmask({
                "mask": "+7 (999) 999-9999",
                oncomplete: () => {
                    this.options.phoneComplete = true;
                },
                onincomplete: () => {
                    this.options.phoneComplete = false;
                }
            }).mask(inputPhone);
        });
    };

    this.init();
}

export default new Registration();