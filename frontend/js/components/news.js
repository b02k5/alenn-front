import { tns } from "../../../node_modules/tiny-slider/src/tiny-slider.module.js";

function News() {
  const slider = document.querySelector('.news');
  if (!slider) return;

  this.html = {
    block: slider,
    list: slider.querySelector('.news__list'),
    slides: slider.querySelectorAll('.news__item'),
  }



  this.init = () => {
    this.createTinySlider();
  }
  this.createTinySlider = () => {

    this.tns = tns({
      container: this.html.list,
      items: 1,
      slideBy: '1',
      autoplay: false,
      mouseDrag: false,
      controls: false,
      nav: false,
      gutter: 30,
      speed: 300,
      loop: false,
      edgePadding: 40,
      responsive: {
        768: {
          disable: true
        }
      }
    });
    this.tns.events.on('indexChanged',function(e){

    })
  }
  this.init();
}

export default new News();