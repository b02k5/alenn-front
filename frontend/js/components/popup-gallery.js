import { tns } from "../../../node_modules/tiny-slider/src/tiny-slider.module.js";
import popup from "./popup.js";

function PopupGallery() {
  const gallery = document.querySelector('.gallery');
  const containerPop = document.querySelector('.popup--gallery');
  const galleryListPopUp = document.querySelector('.popup__toggles');
  const galleryList = document.querySelector('.gallery__list');
  const galleryItems = document.querySelectorAll('.gallery__item');
  const containerImg = document.querySelector('.popup__list-top');
  const popupNav = document.querySelector('.popup__nav');

  this.init = () => {
    if (gallery) {
      this.cloneElements();
      this.openGallery();
    }
  };

  this.cloneElements = () => {
    let allElements = [...galleryItems];
    allElements.map((item, i) => {
      item.setAttribute('id', `tns1-item${i}`);
      galleryListPopUp.appendChild(item.cloneNode(true)).className = 'popup__toggle';
      containerImg.appendChild(item.cloneNode(true)).className = 'popup__item';
    });
  };

  this.clearNavActive = () => {
    const galleryPopUpItems = document.querySelectorAll('.popup__toggle');
    let allItems = [...galleryPopUpItems];

    allItems.map(item => {
      item.classList.contains('tns-nav-active')
        && item.classList.remove('tns-nav-active')
    });
  }

  this.openGallery = () => {
    const slider = this.createTinySlider();
    const sliderToggle = this.createTinySliderToggle();

    galleryList.addEventListener('click', e => {
      this.clearNavActive();
      const current = e.target.closest('.gallery__item');
      const getAttribute = current.getAttribute('id');
      const getNavItem = document.querySelector(`[aria-controls="${getAttribute}"]`);
      if (current) {
        slider.goTo(Number(getNavItem.getAttribute('data-nav')));
        sliderToggle.goTo(Number(getNavItem.getAttribute('data-nav')));
        popup.openPopup(containerPop);
      }
    });
  };

  this.createTinySlider = () => {
    return tns({
      container: containerImg,
      item: 1,
      controlsContainer: popupNav,
      navContainer: galleryListPopUp,
      speed: 400,
      swipeAngle: false
    });
  };

  this.createTinySliderToggle = () => {
    return tns({
      container: galleryListPopUp,
      items: 3,
      controlsContainer: popupNav,
      navContainer: containerImg,
      nav: false,
      mouseDrag: true,
      swipeAngle: false,
      speed: 400,
      responsive: {
        500: {
          items: 5,
        },
        700: {
          items: 6
        },
        900: {
          items: 9
        }
      }
    });
  };

  this.init();
}

export default new PopupGallery();