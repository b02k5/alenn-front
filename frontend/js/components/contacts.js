function map() {
    //const map = document.querySelectorAll('.map-google');

    var maps = document.getElementsByClassName('map-google');


    if (maps.length == 0){
        return false
    }
    var googleMaps = [];
    var googleMarkers = [];

    const key = maps[0].getAttribute('data-key');
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "https://maps.googleapis.com/maps/api/js?key=" + key + "&callback=window.initMaps";
    document.body.appendChild(script);

    window.initMaps = function() {
        for(let i = 0; i < maps.length; i++){
            let mapCenter = maps[i].getAttribute('data-center').split(",");
            let mapZoom = +maps[i].getAttribute('data-zoom');


            googleMaps[i] = new google.maps.Map(maps[i], {
                zoom: (mapZoom && mapZoom != 0) ? mapZoom : 14,
                center: {lat: +mapCenter[0], lng: +mapCenter[1]},
                disableDefaultUI: true,
            });

            googleMarkers[i] = new google.maps.Marker({
                position: {
                    lat: +mapCenter[0],
                    lng: +mapCenter[1]
                },
                map: googleMaps[i],
            });


            window.addEventListener('resize', function() {
                setTimeout(function() {
                    googleMaps[i].setCenter({lat: +mapCenter[0], lng: +mapCenter[1]});
                }, 10);
            });
        }

    }

}


export default map();