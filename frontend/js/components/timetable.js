import { tns } from "../../../node_modules/tiny-slider/src/tiny-slider.module.js";
import { log } from "util";

function Timetable() {
  const slider = document.querySelector('.timetable');
  if (!slider) return;

  this.html = {
    block: slider,
    list: slider.querySelector('.timetable__list'),
    navBlock: slider.querySelector('.slider__controls'),
  }

  this.init = () => {
    if (this.html.list.childElementCount <= 1)
      return;
    this.createTinySlider();
  };
  log
  this.createTinySlider = (list) => {
    this.tns = tns({
      container: this.html.list,
      items: 1,
      slideBy: '1',
      autoplay: false,
      mouseDrag: false,
      controls: true,
      controlsContainer: this.html.navBlock,
      nav: false,
      speed: 300,
      loop: false,
      arrowKeys: true,
    });
  };



  this.init();
}

export default new Timetable();