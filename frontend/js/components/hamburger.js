(function() {
    const hamburgerToggle = document.querySelector('.hamburger');
    const headerMenu = document.querySelector('.page-header__menu');
    const filter = document.querySelector('.page-header__search');

    if (!hamburgerToggle || !headerMenu) return;

    headerMenu.classList.remove('no-js');

    hamburgerToggle.onclick = function(e) {
        e.preventDefault();
        headerMenu.classList.toggle('page-header__menu--open');
        filter.classList.toggle('hide');
        this.classList.toggle('hamburger--open');
    }
})();