import Choices from "../../../node_modules/choices.js/assets/scripts/dist/choices.min.js";
import {ajax, wordsDeclension} from './common.js';



function Filter(options) {
  const block = document.querySelector('.filter');
  const result = document.querySelector('.result');
  if (!block || !result) return;

  this.html = {
    block: block,
    form: block.querySelector('.filter__form'),
    selects: block.querySelectorAll('select'),
    result: result,
    resultHeader: result.querySelector('.result__header'),
    resultList: result.querySelector('.result__list'),
    resultLoad: result.querySelector('.result__more')
  }
  this.options = {
    resultPage: 0,
    resultItemsPerPage: options.itemCount || 5,
  }



  this.init = () => {
    this.customSelects();
    this.createTemplate();

    // this.html.resultLoad.onclick = (e) => {
    //   e.preventDefault();
    //   this.loadMore();
    // }

    this.html.block.classList.remove('no-js');
  }



  this.customSelects = () => {
    this.choices = [];

    for (let i=0; i<this.html.selects.length; i++) {
      const select = this.html.selects[i];
      const choice = new Choices(select, {
        searchEnabled: false,
        itemSelectText: '',
        shouldSort: false,


      })
      this.choices.push(choice);

      select.addEventListener('change', () => {
        //this.resetResult();
      });
    }
  }



  this.createTemplate = () => {
    this.templateItem = options.itemTemplateFunction();
  }
  


  this.resetResult = () => {
    let data = new FormData(this.html.form);
    data.append("page", this.options.resultPage);

    ajax(this.html.form.method || 'GET', this.html.form.action, this.setResult, data);
  }
  this.setResult = (response) => {
    return false;
    const obj = JSON.parse(response);
    const dataArr = obj.data || [];
    const items = (!dataArr || !dataArr[0]) ? [] : dataArr[0];
    const count = obj.count;
    const more = obj.more;


    this.options.resultPage = 0;
    this.html.resultList.innerHTML = '';
    this.html.resultHeader.innerHTML = count + ' ' + wordsDeclension(count, ['результат', 'результата', 'результатов']);

    this.addResultItems(items);
    this.checkLoadMore(more);
  }
  this.addResultItems = (items) => {
    for (let itemId in items) {
      const li = document.createElement('li');
      li.classList.add('result__item');
      li.appendChild(this.addResultItem(items[itemId]));
      this.html.resultList.appendChild(li);
    }
  }
  this.addResultItem = options.addResultItem.bind(this);


  this.loadMore = () => {
    this.options.resultPage++;
    let data = new FormData(this.html.form);
    data.append("page", this.options.resultPage);
    ajax(this.html.form.method || 'GET', this.html.form.action, this.appendMoreResultItems, data);
  }
  this.appendMoreResultItems = (response) => {
    const obj = JSON.parse(response);
    const dataArr = obj.data || [];
    const items = (!dataArr || !dataArr[0]) ? [] : dataArr[0];
    const more = obj.more;

    this.addResultItems(items);
    this.checkLoadMore(more);
  }
  this.checkLoadMore = (status) => {
    if (status) {
      this.html.resultLoad.classList.remove('result__more--hidden');
    } else {
      this.html.resultLoad.classList.add('result__more--hidden');
    }
  }



  this.init();
}


export default Filter;