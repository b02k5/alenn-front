import {clickOutside} from "./common.js";

function Popup() {
  const block = document.querySelector('.popup__overlay');
  if (!block) return;

  this.html = {
    overlay: block,
    popups: block.querySelectorAll('.popup'),
    closeBtns: block.querySelectorAll('.popup__close'),
  };

  this.opts = {
    openPopups: [],
  };

  this.init = () => {
    this.initCloseBtns();
  };

  this.initCloseBtns = () => {
    Array.from(this.html.closeBtns).forEach((btn, i) => {
      btn.onclick = (e) => {
        e.preventDefault();
        this.closePopup(this.html.popups[i]);
      }
    })
  };

  this.firstOpen = () => {
    this.html.overlay.classList.add('popup__overlay--show');
    document.body.classList.add('fixed');
    setTimeout(() => {
      window.addEventListener('keydown', this.closeByEsc);
      window.addEventListener('click', this.clickOutside);
    }, 10);

  };

  this.lastClose = () => {
    this.html.overlay.classList.remove('popup__overlay--show');
    document.body.classList.remove('fixed');
    window.removeEventListener('keydown', this.closeByEsc);
    window.removeEventListener('click', this.clickOutside);
  };

  this.openPopup = (popup) => {
    if (this.opts.openPopups.length === 0) {
      this.firstOpen();
    }

    popup.classList.add('popup--active');
    let input = popup.querySelector('input');
    if (input) input.focus();

    this.opts.openPopups.push(popup);
    this.checkOpenedPopups();
  };

  this.closePopup = (popup) => {

    popup.classList.remove('popup--active');
    this.opts.openPopups.pop();
    this.checkOpenedPopups();
  };

  this.checkOpenedPopups = () => {
    if (this.opts.openPopups.length <= 0) {
      this.lastClose();
    }
  };

  this.closeByEsc = (e) => {
    if (e.keyCode === 27) this.closePopup(this.opts.openPopups[this.opts.openPopups.length - 1]);
  };

  this.clickOutside = (e) => {
    if (!clickOutside(e, 'popup')) this.closePopup(this.opts.openPopups[this.opts.openPopups.length - 1]);
  };

  this.init();
}

export default new Popup();