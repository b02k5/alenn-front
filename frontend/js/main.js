import './components/result-subject.js';
import './components/main-slider.js';
import './components/announcements.js';
import Filter from './components/filter.js';
import './components/tab-block.js';
import './components/documents.js';
import './components/news.js';
import './components/achievements.js';
import './components/hamburger.js';
import './components/timetable.js';
import './components/gallery-group.js';
import './components/contacts.js';
import './components/registration.js';
import './components/popup.js';
import './components/popup-registration.js';
import './components/popup-gallery.js';
import './components/archive.js';
import './components/direction.js';

// Фильтры
function filterInit() {

    const result = document.querySelector('.result');
    if (!result) return;
    const filterType = result.getAttribute('data-type');
    let itemTemlateFunction;
    let addResultItem;
    let itemCount = 5;

    if (filterType == 'direction') {
        itemTemlateFunction = function () {
            const block = document.createElement('div');
            block.classList.add('result-subject');

            const rowMain = document.createElement('div');
            rowMain.classList.add('result-subject__info');

            const name = document.createElement('p');
            name.classList.add('result-subject__parameter');
            name.classList.add('result-subject__parameter--name');
            const nameLink = document.createElement('a');
            nameLink.classList.add('result-subject__parameter-link');
            name.appendChild(nameLink);
            rowMain.appendChild(name);

            const classValue = document.createElement('p');
            classValue.classList.add('result-subject__parameter');
            classValue.classList.add('result-subject__parameter--class');
            rowMain.appendChild(classValue);

            const difficulty = document.createElement('p');
            difficulty.classList.add('result-subject__parameter');
            difficulty.classList.add('result-subject__parameter--level');
            rowMain.appendChild(difficulty);

            const openToggle = document.createElement('div');
            openToggle.classList.add('result-subject__toggle');
            const hidden = document.createElement('span');
            hidden.classList.add('visually-hidden');
            hidden.innerHTML = 'Открыть подробную информацию';
            let svgOpen = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            svgOpen.setAttributeNS(null, "class", "result-subject__toggle-icon");
            svgOpen.setAttributeNS(null, "width", "24");
            svgOpen.setAttributeNS(null, "height", "16");
            let useOpen = document.createElementNS("http://www.w3.org/2000/svg", "use");
            useOpen.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#icon-dropdown");
            svgOpen.appendChild(useOpen);
            openToggle.appendChild(hidden);
            openToggle.appendChild(svgOpen);
            rowMain.appendChild(openToggle);
            block.appendChild(rowMain);



            const rowDescription = document.createElement('div');
            rowDescription.classList.add('result-subject__additional-info');

            const description = document.createElement('p');
            description.classList.add('result-subject__text');
            rowDescription.appendChild(description);


            const buttonWrapper = document.createElement('div');
            buttonWrapper.classList.add('result-subject__link-wrapper');
            buttonWrapper.classList.add('button__wrapper');
            const buttonLink = document.createElement('a');
            buttonLink.classList.add('button');
            buttonLink.innerHTML = 'Подробнее';
            const buttonDecoration = document.createElement('div');
            buttonDecoration.classList.add('button__decoration');
            buttonLink.appendChild(buttonDecoration);
            buttonWrapper.appendChild(buttonLink);
            rowDescription.appendChild(buttonWrapper);
            block.appendChild(rowDescription);

            return block;
        }

        addResultItem = function (item) {
            return item;

            // const value = item;
            // const template = this.templateItem.cloneNode(true);
            //
            //
            // const rowMain = template.querySelector('.result-subject__info');
            // const directionValues = template.querySelectorAll('.result-subject__parameter');
            // const toggleOpen = template.querySelector('.result-subject__toggle');
            //
            // directionValues[0].querySelector('.result-subject__parameter-link').innerHTML = value.name;
            // directionValues[0].querySelector('.result-subject__parameter-link').href = value.link;
            // directionValues[1].innerHTML = value.class + ' класс';
            //
            // directionValues[2].classList.add('result-subject__parameter--level' + value.level);
            // directionValues[2].innerHTML = value.lavelName || "Уровень";
            //
            // rowMain.onclick = function(e) {
            //   e.preventDefault();
            //   template.classList.toggle('result-subject--open');
            // }
            //
            //
            // const rowDescription = template.querySelector('.result-subject__additional-info');
            // const description = template.querySelector('.result-subject__text');
            // description.parentNode.removeChild(description);
            //
            // for (let i=0; i<value.description.length; i++) {
            //   const valueDescription = value.description[i];
            //   description.innerHTML = valueDescription;
            //   rowDescription.appendChild(description.cloneNode(true));
            // }
            //
            //
            // const linkWrapper = template.querySelector('.result-subject__link-wrapper');
            // const link = template.querySelector('.button');
            // link.href = value.link;
            // rowDescription.appendChild(linkWrapper);


            //return template;
        }
    }



    if (filterType == 'news') {
        itemTemlateFunction = () => {
            const item = document.createElement('div');
            item.classList.add('news-card');

            const a = document.createElement('a');
            a.classList.add('news-card__link');

            const imgWrapper = document.createElement('div');
            imgWrapper.classList.add('news-card__img-wrapper');

            const img = document.createElement('img');
            img.classList.add('news-card__img');
            imgWrapper.appendChild(img);
            a.appendChild(imgWrapper);

            const time = document.createElement('time');
            time.classList.add('news-card__date');
            a.appendChild(time);

            const name = document.createElement('p');
            name.classList.add('news-card__name');
            a.appendChild(name);

            item.appendChild(a);

            return item;
        }

        addResultItem = function (item) {
            const value = item;
            const template = this.templateItem.cloneNode(true);

            const link = template.querySelector('.news-card__link');
            link.href = value.link;

            const img = template.querySelector('.news-card__img');
            img.src = value.img;
            img.alt = value.name;

            const time = template.querySelector('.news-card__date');
            time.innerHTML = value.date;

            const name = template.querySelector('.news-card__name');
            name.innerHTML = value.name;


            return template;
        }
    }



    if (filterType == 'events') {
        itemTemlateFunction = function () {
            const item = document.createElement('div');
            item.classList.add('event-card');

            const a = document.createElement('a');
            a.classList.add('event-card__link');

            const imgWrapper = document.createElement('div');
            imgWrapper.classList.add('event-card__img-wrapper');

            const img = document.createElement('img');
            img.classList.add('event-card__img');
            imgWrapper.appendChild(img);
            a.appendChild(imgWrapper);

            const info = document.createElement('div');
            info.classList.add('event-card__info');

            const time = document.createElement('time');
            time.classList.add('event-card__date');
            info.appendChild(time);

            const name = document.createElement('p');
            name.classList.add('event-card__name');
            info.appendChild(name);

            const description = document.createElement('p');
            description.classList.add('event-card__description');
            info.appendChild(description);
            a.appendChild(info);
            item.appendChild(a);

            return item;
        }
        addResultItem = function (item) {
            const value = item;
            const template = this.templateItem.cloneNode(true);


            const link = template.querySelector('.event-card__link');
            link.href = value.link;

            const img = template.querySelector('.event-card__img');
            img.src = value.img;
            img.alt = value.name || 'Картинка новости';

            const time = template.querySelector('.event-card__date');
            time.innerHTML = value.date;

            const name = template.querySelector('.event-card__name');
            name.innerHTML = value.name;

            const description = template.querySelector('.event-card__description');
            description.innerHTML = value.description;

            return template;
        }
    }
    this.init = function () {
        new Filter({
            itemTemplateFunction: itemTemlateFunction,
            addResultItem: addResultItem,
            itemCount: itemCount
        })
    }
}
window.onload = function () {
    const result = document.querySelector('.result');
    if (!result) return;
    filter.init()
};
window.filter = new filterInit();


// (function() {
//
//
//
//
//
//
//
//   new Filter({
//     itemTemplateFunction: itemTemlateFunction,
//     addResultItem: addResultItem,
//     itemCount: itemCount,
//   })
//
// })();
